﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using System.Data.SqlClient;
using System.Data;
using Prj_WebService.Models;
using System.Web.Script.Serialization;   //** 自己宣告、加入。JSON會用到。JavaScriptSerializer

using Newtonsoft.Json;   //** 自己宣告、加入。JSON.NET會用到。

namespace Prj_WebService.Controllers
{
    public class JSONController : Controller
    {
        private MVC_UserDBContext _db = new MVC_UserDBContext();
        // GET: JSON
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();  //***這裡需要自己修改，例如 _db字樣
            }
            base.Dispose(disposing);
        }
        public ActionResult Index_JSON1()
        {
            var data = from u in _db.UserTables
                       select new
                       {
                           UserId = u.UserId,
                           UserName = u.UserName,
                           UserMobilePhone = u.UserMobilePhone
                       };
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string result = serializer.Serialize(data);
            return Content(result);
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}