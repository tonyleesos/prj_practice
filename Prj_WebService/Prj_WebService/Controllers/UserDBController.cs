﻿using Prj_WebService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Prj_WebService.Controllers
{
    public class UserDBController : Controller
    {
        // GET: UserDB
        private MVC_UserDBContext _db = new MVC_UserDBContext();
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            return View();
        }
        public JsonResult List()
        {      //****************
            IQueryable<UserTable> ListAll = _db.UserTables;
            // 或是寫成
            //var ListAll = from m in _db.UserTables
            //                   select m;

            return Json(ListAll.ToList(), JsonRequestBehavior.AllowGet);
            //                                   //*************************
            // 直到程式的最後，把查詢結果 IQueryable呼叫.ToList()時，上面那一段LINQ才會真正被執行！
        }


        //===================================
        //== 列出一筆記錄的明細（Details） ==
        //===================================
        public JsonResult Details(int id)  // 網址 http://xxxxxx/UserDB/Details?_ID=1 
        {   //****************
            // 如果沒有修改路由  /App_Start/RouteConfig.cs，網址輸入 http://xxxxxx/UserDB/Details/2 會報錯！ 
            UserTable ut = _db.UserTables.Find(id);

            return Json(ut, JsonRequestBehavior.AllowGet);
            //                  //*************************
        }
    }
}