﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Prj_WebService.Models;

namespace Prj_WebService.Controllers
{
    public class WebAPIController : ApiController
    {
        private MVC_UserDBContext _db = new MVC_UserDBContext();
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
        public IEnumerable<UserTable> GerList()
        {
            //IQueryable<UserTable> Listall = from m in _db.UserTables select m;
            IQueryable<UserTable> Listall = _db.UserTables;
            return Listall.ToList();
        }
        public UserTable GetDetails(int id)
        {
            UserTable ut = _db.UserTables.Find(id);
            return ut;
        }
    }
}
