﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Prj_WebService.ServiceReference1;
using Prj_WebService.ModelsWebService;

namespace Prj_WebService.Controllers
{
    public class WebServiceController : Controller
    {
        // GET: WebService
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult WS1()
        {
            WebService1SoapClient ws = new WebService1SoapClient();
            string str = ws.HelloWorld();
            return Content(str.ToString());
        }
        public ActionResult WS2()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult WS2(WS_Class1 wsClass)
        {
            WebService1SoapClient ws = new WebService1SoapClient();
            string result = ws.ComputelT(wsClass.NumberA, wsClass.NumberB);
            return Content(result);
        }
        public ActionResult WS3_Jquery()
        {
            return View();
        }
    }
}