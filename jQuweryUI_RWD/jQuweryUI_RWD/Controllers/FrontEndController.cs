﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuweryUI_RWD.Controllers
{

    public class FrontEndController : Controller
    {
        public List<Models.Class1> Data_Init()
        {
            List<Models.Class1> y = new List<Models.Class1>();
            y.Add(new Models.Class1
            {
                MyID = 11,
                MyName = "Tony"
            });
            y.Add(new Models.Class1
            {
                MyID = 12,
                MyName = "Andy"
            });
            y.Add(new Models.Class1
            {
                MyID = 13,
                MyName = "Cindy"
            });
            return y.ToList();
        }

        // GET: FrontEnd
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create_dataPicker()
        {
            return View();
        }
        public ActionResult Index_Accordion()
        {
            List<Models.Class1> x = new List<Models.Class1>();
            x.Add(new Models.Class1
            {
                MyID = 11,
                MyName = "Tony"
            });
            x.Add(new Models.Class1
            {
                MyID = 12,
                MyName = "Andy"
            });
            x.Add(new Models.Class1
            {
                MyID = 13,
                MyName = "Cindy"
            });
            return View(x);
        }
        public ActionResult Index_Tabs()
        {
            List<Models.Class1> x = new List<Models.Class1>();
            x.Add(new Models.Class1
            {
                MyID = 11,
                MyName = "Tony",
                MyBirthDay = Convert.ToDateTime("2010/12/12")
            });
            x.Add(new Models.Class1
            {
                MyID = 12,
                MyName = "Andy",
                MyBirthDay = Convert.ToDateTime("2019/12/12")
            });
            x.Add(new Models.Class1
            {
                MyID = 13,
                MyName = "Cindy",
                MyBirthDay = Convert.ToDateTime("2018/11/12")
            });
            return View(x);
        }
        public ActionResult Index_RWD_Template()
        {
            return View();
        }
        public ActionResult Index_RWD_ZeroTemplate()
        {
            return View();
        }
        public ActionResult Index_RWD_MvcDeflult()
        {
            return View(Data_Init());
        }
        public ActionResult Index_RWD_Carousel()
        {
            return View(Data_Init());
        }
        public ActionResult Index_RWD_Carousel3()
        {
            return View(Data_Init());
        }
        [ValidateInput(false)]
        public ActionResult Create_CKeditor()
        {
            return View();
        }
        public ActionResult List_fancyBox3()
        {
            return View(Data_Init());
        }
        public ActionResult GoogleChart_Pie()
        {
            return View();   // 空白畫面（不需 jQuery）
            // https://developers.google.com/chart/interactive/docs/gallery/piechart
        }
        public ActionResult GoogleChart_GeoMap()
        {
            // https://developers.google.com/chart/interactive/docs/gallery/geochart
            return View();   // 空白畫面（不需 jQuery）
        }
        public ActionResult GoogleChart_Geo()
        {
            // https://developers.google.com/chart/interactive/docs/gallery/geochart
            return View();   // 空白畫面（不需 jQuery）
        }
        public ActionResult GoogleChart_Pie2_ViewData()
        {
            ViewData["RawData"] = @"[
                                                            ['Task', 'Hours per Day'],
                                                            ['Work', 11],
                                                            ['Eat', 2],
                                                            ['Commute',2],
                                                            ['Watch TV',2],
                                                            ['Sleep',7]
                                                        ]";
            return View();   // 空白畫面（不需 jQuery）
        }
    }
}