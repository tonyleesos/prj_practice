﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuweryUI_RWD.Controllers
{
    public class JavaScriptController : Controller
    {
        // GET: JavaScript
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult JavaScript_Test1()
        {
            string urlStr = Url.Action("Index");
            string jsCode = $"<script> alert(\"找不到資料!\");location.href=\"{urlStr}\";</script>";

            return Content(jsCode);
        }
        public ActionResult JavaScript_Test2()
        {
            string urlStr = Url.Action("Index");
            string jsCode = $"alert('找不到資料!');location.href='{urlStr}';";
            TempData["jsMessage"] = jsCode;
            return View();
        }
        public ActionResult JavaScript_Test4()
        {
            return View();
        }
        public ActionResult JavaScriptMessage2()
        {
            string urlStr = Url.Action("Index");
            string jsCode = $"alert('找不到資料!')";
            return JavaScript(jsCode);
        }
    }
}