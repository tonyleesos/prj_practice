﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace jQuweryUI_RWD.Models
{
    public class Class1
    {
        public int MyID { get; set; }
        [AllowHtml]
        public string MyName { get; set; }
        public DateTime MyBirthDay { get; set; }
        public bool MyApproved { get; set; }  // Checkbox
        public bool? MyDropDownList { get; set; }  // DropDownList

    }
}