namespace jQuweryUI_RWD.ModelsPhoto
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Comment
    {
        [Key]
        public int CommentID { get; set; }

        public int PhotoID { get; set; }

        public string UserName { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public virtual Photo Photos { get; set; }
    }
}
