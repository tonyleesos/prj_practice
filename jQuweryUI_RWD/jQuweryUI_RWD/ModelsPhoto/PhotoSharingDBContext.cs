namespace jQuweryUI_RWD.ModelsPhoto
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class PhotoSharingDBContext : DbContext
    {
        public PhotoSharingDBContext()
            : base("name=PhotoSharingDBContext")
        {
        }

        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Photo> Photos { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
