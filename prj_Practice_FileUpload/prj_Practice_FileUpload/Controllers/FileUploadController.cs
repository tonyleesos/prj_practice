﻿using prj_Practice_FileUpload.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace prj_Practice_FileUpload.Controllers
{
    public class FileUploadController : Controller
    {
        private File_UploadContext _db = new File_UploadContext();

        // GET: FileUpload
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();  //***這裡需要自己修改，例如 _db字樣
            }
            base.Dispose(disposing);
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Create()   // 基本款
        {   // 檢視畫面，為了「檔案上傳」，有些地方要自己動手修改
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]   // 避免XSS、CSRF攻擊
        public ActionResult Create(HttpPostedFileBase FileUpload_FileName)   // 基本款
        {   // 檢視畫面，為了「檔案上傳」，有些地方要自己動手修改
            if (!ModelState.IsValid) return View();
            #region
            // 方法一
            //try
            //{
            //    if (FileUpload_FileName.ContentLength > 0)   // 檢查 <input type="file"> 是否輸入檔案？
            //    {
            //        string _FileName = Path.GetFileName(FileUpload_FileName.FileName);   // 需搭配 System.IO命名空間

            //        string _path = "d:\\temp\\uploads\\";      // 或是寫成  string _path = @"d:\temp\uploads\";

            //        // 透過 Path.Combine()處理目錄與路徑最後的「斜線」問題  
            //        string _end = Path.Combine(_path, _FileName);

            //        FileUpload_FileName.SaveAs(_end);   // 上傳完畢，並且把檔案存放在 /Images目錄底下
            //        // 上傳後，檔案存放在Web Server的硬碟裡面，需要「（目錄）路徑」、「檔名」兩大條件！

            //        ViewBag.Message = "上傳成功。完整路徑與檔名：" + _end;
            //    }
            //    else
            //        ViewBag.Message = "您尚未挑選檔案，無法上傳";
            //}
            //catch
            //{
            //    ViewBag.Message = "上傳失敗。File upload failed!!";
            //}
            //return View();
            #endregion

            #region
            try
            {
                if (FileUpload_FileName.ContentLength > 0)
                {
                    string _FileName = Path.GetFileName(FileUpload_FileName.FileName);
                    string appPath = Request.PhysicalApplicationPath;
                    string saveDir = "Images\\";
                    string _path = Path.Combine(appPath, saveDir);
                    string _end = Path.Combine(_path, _FileName);
                    FileUpload_FileName.SaveAs(_end);
                    ViewBag.Message = "上傳成功。完整路徑與檔名：" + _end;
                }
                else
                    ViewBag.Message = "您尚未挑選檔案，無法上傳";
            }
            catch
            {
                ViewBag.Message = "上傳失敗。File upload failed!!";
            }

            return View();
            #endregion
        }
        public ActionResult Create_ReName()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]   // 避免XSS、CSRF攻擊
        public ActionResult Create_ReName(HttpPostedFileBase FileUpload_FileName)   // 基本款
        {   // 檢視畫面，為了「檔案上傳」，有些地方要自己動手修改
            if (!ModelState.IsValid) return View();
            if (FileUpload_FileName.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(FileUpload_FileName.FileName);
                string appPath = Request.PhysicalApplicationPath;
                string saveDir = "Images\\";
                string _path = Path.Combine(appPath, saveDir);
                //
                string pathToCheck = Path.Combine(_path, _FileName);
                string tempfileName = "";

                //重副檔名處理 方法一
                #region
                if (System.IO.File.Exists(pathToCheck))
                {
                    int counter = 2;
                    while (System.IO.File.Exists(pathToCheck)) //如果找到相同檔名
                    {
                        tempfileName = counter.ToString() + "_" + _FileName;
                        pathToCheck = Path.Combine(_path, tempfileName);
                        counter++;
                    }
                    string str = "抱歉，你上船的檔名發生衝突，檔名修正為" + tempfileName;
                }
                else
                {
                    tempfileName = _FileName;
                }
                #endregion               

            }
            return View();
        }
        public ActionResult Create_ReName2()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]   // 避免XSS、CSRF攻擊
        public ActionResult Create_ReName2(HttpPostedFileBase FileUpload_FileName)   // 基本款
        {   // 檢視畫面，為了「檔案上傳」，有些地方要自己動手修改
            if (!ModelState.IsValid) return View();
            if (FileUpload_FileName.ContentLength > 0)
            {
                string _FileName = Path.GetFileName(FileUpload_FileName.FileName);
                //string appPath = Request.PhysicalApplicationPath;
                string saveDir = "Images\\";
                string appPath = WebConfigurationManager.ConnectionStrings["FileUpload_Dir"].ConnectionString;  // 改寫webConfigure
                string _path = Path.Combine(appPath, saveDir);
                //
                string pathToCheck = Path.Combine(_path, _FileName);
                string tempfileName = "";

                //重副檔名處理 方法二
                #region
                if (System.IO.File.Exists(pathToCheck))
                {
                    string OnlyFileName = Path.GetFileNameWithoutExtension(_FileName);   //主檔名
                    string fileExtension = Path.GetExtension(_FileName);  //副檔名
                    int counter = 2;
                    while (System.IO.File.Exists(pathToCheck)) //如果找到相同檔名
                    {
                        _FileName = OnlyFileName + "_" + counter.ToString() + fileExtension;  //ABC1.jpg 、 ABC2.jpg                        
                        pathToCheck = Path.Combine(_path, _FileName);
                        counter++;
                    }
                    string str = "抱歉，你上傳的檔名發生衝突，檔名修正為" + tempfileName;
                }

                #endregion
                string _end = Path.Combine(_path, _FileName);
                FileUpload_FileName.SaveAs(_end);   // 上傳完畢
            }
            return View();
        }

        public ActionResult Create_DB1()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]   // 避免XSS、CSRF攻擊
        public ActionResult Create_DB1(FileUpload_DB _DB, HttpPostedFileBase FileUpload_FileName)
        {
            string strMessage = "";
            string _FileName = "";
            try
            {
                if (FileUpload_FileName.ContentLength > 0)
                {
                    _FileName = Path.GetFileName(FileUpload_FileName.FileName);
                    //string appPath = "G:\\temp\\";
                    string appPath = Request.PhysicalApplicationPath;

                    //string appPath = WebConfigurationManager.ConnectionStrings["FileUpload_Dir"].ConnectionString;  // 改寫webConfigure

                    string _end = Path.Combine(appPath, _FileName);
                    FileUpload_FileName.SaveAs(_end);
                    strMessage += "上傳成功，請檢查 (1) FileUpload_DB資料表與 (2)上傳的目錄。<br>完整路徑與檔名：" + _end;
                }
                else
                    strMessage += "您尚未挑選檔案，無法上傳";
            }
            catch
            {
                strMessage += "上傳失敗。File upload failed!!";
            }
            // Create DB
            _DB.FileUpload_FileName = _FileName;
            if ((_DB != null) && (ModelState.IsValid))
            {
                _db.FileUpload_DBs.Add(_DB);
                _db.SaveChanges();
                strMessage += "<h3>寫入FileUpload_DB資料表</h3>";
            }
            return Content(strMessage);
        }
    }
}