namespace prj_Practice_FileUpload.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FileUpload_DB2
    {
        [Key]
        public int FileUpload_DB_id { get; set; }

        public DateTime? FileUpload_time { get; set; }

        public int? test_id { get; set; }

        [Column(TypeName = "image")]
        public byte[] FileUpload_FileName { get; set; }

        [StringLength(25)]
        public string FileUpload_MIME { get; set; }

        [StringLength(250)]
        public string FileUpload_Memo { get; set; }

        [StringLength(50)]
        public string FileUpload_User { get; set; }
    }
}
