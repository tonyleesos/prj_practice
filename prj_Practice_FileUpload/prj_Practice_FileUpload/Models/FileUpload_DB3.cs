namespace prj_Practice_FileUpload.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class FileUpload_DB3
    {
        [Key]
        public int FileUpload_DB_id { get; set; }

        public DateTime? FileUpload_time { get; set; }

        public byte[] FileUpload_FileName { get; set; }
    }
}
