namespace prj_Practice_FileUpload.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class File_UploadContext : DbContext
    {
        public File_UploadContext()
            : base("name=File_UploadContext")
        {
        }

        public virtual DbSet<FileUpload_DB> FileUpload_DBs { get; set; }
        public virtual DbSet<FileUpload_DB2> FileUpload_DB2s { get; set; }
        public virtual DbSet<FileUpload_DB3> FileUpload_DB3s { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
