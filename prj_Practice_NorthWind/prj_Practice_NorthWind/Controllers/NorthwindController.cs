﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj_Practice_NorthWind.Models;
using Xceed.Document.NET;
using Xceed.Words.NET;

namespace prj_Practice_NorthWind.Controllers
{
    public class NorthwindController : Controller
    {
        public Northwind _db = new Northwind();
        // GET: Northwind
        public ActionResult IndexJoin()
        {
            var result = from od in _db.Order_Details
                         join o in _db.Orders on od.OrderID equals o.OrderID into odo
                         from o in odo.DefaultIfEmpty()
                         select new OrderDetails_OrdersViewModel { Order = o, OrderDetails = od };
            return View(result);
        }
        public ActionResult IndexVM()
        {
            List<Order_OrederDetialsListViewModel> result = new List<Order_OrederDetialsListViewModel>();
            // 一(第一層)對多
            foreach (var o in _db.Orders)
            {
                //會有好幾筆Order_Details
                List<Order_Details> od = new List<Order_Details>();
                foreach (var item in _db.Order_Details.Where(x => x.OrderID == o.OrderID))
                {
                    od.Add(item);
                }

                result.Add(new Order_OrederDetialsListViewModel { order = o, order_details = od });

            }

            // DrowDownList
            var selectList = new List<SelectListItem>()
            {
                new SelectListItem {Text="text-1", Value="value-1" },
                new SelectListItem {Text="text-2", Value="value-2" },
                new SelectListItem {Text="text-3", Value="value-3" },
            };

            //預設選擇哪一筆
            selectList.Where(q => q.Value == "value-2").First().Selected = true;

            ViewBag.SelectList = selectList;

            return View(result.ToList());
        }
        public ActionResult EmployeeIndex()
        {
            var Employee = _db.Employee;
            return View(Employee);
        }
        public ActionResult EmployeeCreate()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmployeeCreate(Employee employeesData)
        {
            if (employeesData == null)
            {   // 沒有輸入內容，就會報錯 - Bad Request
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                _db.Employee.Add(employeesData);
                _db.SaveChanges();
                return RedirectToAction("EmployeeIndex");
            }
            else
            {
                ModelState.AddModelError("Value1", " 自訂錯誤訊息(1) ");
                ModelState.AddModelError("Value2", " 自訂錯誤訊息(2) ");
                return View();
            }
        }
        public ActionResult EmployeeEdit(int id)
        {
            if (id == 0)
            {
                Employee employeesData = new Employee();
                return View(employeesData);
            }
            else
            {
                Employee employeesData = _db.Employee.Find(id);
                return View(employeesData);
            }

        }
        public ActionResult EmployeeView(int id)
        {
            if (id == 0)
            {
                Employee employeesData = new Employee();
                return View(employeesData);
            }
            else
            {
                Employee employeesData = _db.Employee.Find(id);
                ViewBag.employeesInfoData = _db.EmployeeInformation.Where(x => x.EmployeeID == id).ToList();
                return View(employeesData);
            }

        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmployeeEdit(Employee employeesData)
        {
            if (employeesData == null)
            {   // 沒有輸入內容，就會報錯 - Bad Request
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                Employee employees = _db.Employee.Find(employeesData.EmployeeID);

                employees.FirstName = employeesData.FirstName;
                employees.LastName = employeesData.LastName;
                employees.Address = employeesData.Address;
                employees.City = employeesData.City;
                employees.HomePhone = employeesData.HomePhone;
                employees.BirthDate = employeesData.BirthDate;
                employees.PostalCode = employeesData.PostalCode;

                _db.SaveChanges();
                return RedirectToAction("EmployeeIndex");
            }
            else
            {
                ModelState.AddModelError("Value1", " 自訂錯誤訊息(1) ");
                ModelState.AddModelError("Value2", " 自訂錯誤訊息(2) ");
                return View();
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EmployeeDelete(int id)
        {
            if (ModelState.IsValid)
            {
                Employee DeleteItem = _db.Employee.Find(id);
                _db.Employee.Remove(DeleteItem);
                _db.SaveChanges();
                return RedirectToAction("EmployeeIndex");
            }
            else
            {
                ModelState.AddModelError("Value1", " 自訂錯誤訊息(1) ");
                ModelState.AddModelError("Value2", " 自訂錯誤訊息(2) ");
                return View();
            }

        }

        [HttpPost]
        public JsonResult GetServiceQuote(int id = 0)
        {
            Employee vm = new Employee();
            var data = _db.Employee;
            var model = _db.Employee.Where(x => x.EmployeeID == id).FirstOrDefault();
            if (model.EmployeeID != null)
            {
                vm.LastName = model.LastName;
                vm.FirstName = model.FirstName;
                vm.Address = model.Address;
                vm.City = model.City;
            }

            var EmployeeCheckboxLabel = new List<string>();
            if (id == 5)
            {
                EmployeeCheckboxLabel.Add("陸軍");
                EmployeeCheckboxLabel.Add("海軍");
                EmployeeCheckboxLabel.Add("空軍");
                EmployeeCheckboxLabel.Add("陸戰隊");
            }
            if (id == 6)
            {
                EmployeeCheckboxLabel.Add("陸軍");
                EmployeeCheckboxLabel.Add("海軍");
            }
            if (id == 7)
            {
                EmployeeCheckboxLabel.Add("陸軍");
            }
            vm.EmployeeCheckboxLabel = EmployeeCheckboxLabel;

            //==============================================================================================================
            var EmployeeCheckbox = new List<MyListItem>();
            if (id == 5)
            {
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 1, CheckboxName = "測試1", IsChecked = false });
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 2, CheckboxName = "測試2", IsChecked = false });
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 3, CheckboxName = "測試3", IsChecked = false });
            }
            else if (id == 6)
            {
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 1, CheckboxName = "測試1", IsChecked = false });
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 2, CheckboxName = "測試2", IsChecked = false });
            }
            else if (id == 7)
            {
                EmployeeCheckbox.Add(new MyListItem { Checkboxid = 1, CheckboxName = "測試1", IsChecked = false });
            }

            EmployeeEntity res = new EmployeeEntity { EmplyeeCheckbox = EmployeeCheckbox };
            vm.MyCheckBox = res;

            return Json(vm, JsonRequestBehavior.AllowGet);

        }

        public ActionResult ExportWord()
        {
            string fileName = @"G:\prj_practice\prj_Practice_NorthWind\prj_Practice_NorthWind\WordTemplate\employeeTemplate.docx";
            string handle = Guid.NewGuid().ToString();
            List<Employee> EmployeeList = _db.Employee.ToList();
            using (DocX document = DocX.Load(fileName))
            {
                int i = 0;
                int j = 0;
                foreach (var Employee in EmployeeList)
                {
                    Table tb = document.AddTable(2, 4);
                    tb.Design = TableDesign.LightGridAccent5;//套上的表格格式
                    tb.Alignment = Alignment.center;//至中格式
                    // Title
                    tb.Rows[0].Cells[0].Paragraphs[0].Append("LastName");
                    tb.Rows[1].Cells[0].Paragraphs[0].Append("FirstName");
                    tb.Rows[0].Cells[2].Paragraphs[0].Append("Address");
                    tb.Rows[1].Cells[2].Paragraphs[0].Append("City");

                    // Value
                    tb.Rows[0].Cells[1].Paragraphs[0].Append(Employee.LastName);
                    tb.Rows[1].Cells[1].Paragraphs[0].Append(Employee.FirstName);
                    tb.Rows[0].Cells[3].Paragraphs[0].Append(Employee.Address);
                    tb.Rows[1].Cells[3].Paragraphs[0].Append(Employee.City);
                    var row = tb.InsertRow();//新增列
                    i++;
                    j++;

                    // var pa = document.InsertParagraph(Employee.EmployeeID.ToString());
                    document.InsertTable(tb);
                    //var pa = document.InsertParagraph("插入一個新段落");
                    //pa.InsertTableAfterSelf(tb);
                }


                if (Directory.Exists($"~/WordTemplate/{handle}/{fileName}"))
                {
                    //資料夾存在
                    string fileNamexx = @"G:\prj_practice\prj_Practice_NorthWind\prj_Practice_NorthWind\WordTemplate\" + handle + @"\employeeTemplate_Result" + ".docx";
                    document.SaveAs(fileNamexx);
                }
                else
                {
                    //新增資料夾
                    Directory.CreateDirectory(@"G:\prj_practice\prj_Practice_NorthWind\prj_Practice_NorthWind\WordTemplate\" + handle);
                    string fileNamexx = @"G:\prj_practice\prj_Practice_NorthWind\prj_Practice_NorthWind\WordTemplate\" + handle + @"\employeeTemplate_Result" + ".docx";

                    document.SaveAs(fileNamexx);
                }

            }

            return new JsonResult()
            {
                Data = new { FileGuid = handle, FileName = "employeeTemplate_Result" + ".docx" }
            };
        }
        [HttpGet]
        public ActionResult Download(string fileGuid, string fileName)
        {

            try
            {
                if (Directory.Exists($"~/WordTemplate/{fileGuid}"))
                {
                    string path = Server.MapPath($"~/WordTemplate/{fileGuid}/{fileName}");
                    //資料夾存在
                    FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                    return File(stream, "application/octet-stream", fileName); //MME 格式 可上網查 此為通用設定
                }
                else
                {
                    //新增資料夾
                    Directory.CreateDirectory(@"G:\prj_practice\prj_Practice_NorthWind\prj_Practice_NorthWind\WordTemplate\" + fileGuid);
                    string path = Server.MapPath($"~/WordTemplate/{fileGuid}/{fileName}");
                    //資料夾存在
                    FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
                    return File(stream, "application/octet-stream", fileName); //MME 格式 可上網查 此為通用設定
                }
            }
            catch (Exception)
            {
                return Content("<script>alert('查無此檔案');window.close()</script>");
            }


            //try
            //{

            //}
            //catch (System.Exception)
            //{
            //    return Content("<script>alert('查無此檔案');window.close()</script>");
            //}

        }
    }
}