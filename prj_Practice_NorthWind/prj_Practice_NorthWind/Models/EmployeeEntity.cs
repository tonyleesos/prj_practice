﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace prj_Practice_NorthWind.Models
{
    public partial class EmployeeEntity
    {
        public List<MyListItem> EmplyeeCheckbox { get; set; }

    }
    public class MyListItem
    {
        public int Checkboxid { get; set; }
        public string CheckboxName { get; set; }
        public bool IsChecked { get; set; }
    }

}