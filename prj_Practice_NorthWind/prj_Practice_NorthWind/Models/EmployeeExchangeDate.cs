namespace prj_Practice_NorthWind.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmployeeExchangeDate")]
    public partial class EmployeeExchangeDate
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Column("oriStartTime ", TypeName = "datetime2")]
        public DateTime? oriStartTime_ { get; set; }

        [Column("oriEndTime ", TypeName = "datetime2")]
        public DateTime? oriEndTime_ { get; set; }

        [Column("exStartTime ", TypeName = "datetime2")]
        public DateTime? exStartTime_ { get; set; }

        [Column("exEndTime ", TypeName = "datetime2")]
        public DateTime? exEndTime_ { get; set; }

        [Required]
        [StringLength(20)]
        public string Name { get; set; }

        public int? type { get; set; }
    }
}
