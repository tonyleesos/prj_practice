namespace prj_Practice_NorthWind.Models
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmployeeInformation")]
    public partial class EmployeeInformation
    {
        public int Id { get; set; }

        public int? EmployeeTitleID { get; set; }

        public int? EmployeeDepartmentID { get; set; }

        public int? EmployeePositionID { get; set; }

        public int EmployeeID { get; set; }

        public virtual Employee Employee { get; set; }

        public virtual EmployeeDepartment EmployeeDepartment { get; set; }

        public virtual EmployeePosition EmployeePosition { get; set; }

        public virtual EmployeeTitle EmployeeTitle { get; set; }
        [NotMapped]
        public string JsonObject
        {
            get
            {
                return JsonConvert.SerializeObject(new
                {
                    TitleName = EmployeeTitle.EmployeeTitleName,
                    DepartmentName = EmployeeDepartment.EmployeeDepartmentName,
                    PositionName = EmployeePosition.EmployeePositionName,
                    TitleContentId = EmployeeTitle.Id,
                    DepartmentContentId = EmployeeDepartment.Id,
                    PositionContentId = EmployeeDepartment.Id
                });
            }
        }
    }
}
