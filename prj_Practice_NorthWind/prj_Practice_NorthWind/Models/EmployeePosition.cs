namespace prj_Practice_NorthWind.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("EmployeePosition")]
    public partial class EmployeePosition
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public EmployeePosition()
        {
            EmployeeInformation = new HashSet<EmployeeInformation>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string EmployeePositionName { get; set; }

        public int? EmployeeInformationID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EmployeeInformation> EmployeeInformation { get; set; }
    }
}
