﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_Practice_NorthWind.Models
{
    public class Order_OrederDetialsListViewModel
    {
        public Orders order { get; set; }
        public IList<Order_Details> order_details { get; set; }
    }
}