﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace prj_Practice_User
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //*** 搭配 UserDB控制器 *******************************************
            routes.MapRoute(
                name: "Default2",           //*****改這裡！
                url: "{controller}/{action}/{_ID}",                                   //*******改這裡！
                defaults: new { controller = "UserDB", action = "List", _ID = UrlParameter.Optional }
            );

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);
        }
    }
}
