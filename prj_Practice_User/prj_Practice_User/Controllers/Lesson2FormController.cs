﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj_Practice_User.Models;

namespace prj_Practice_User.Controllers
{
    public class Lesson2FormController : Controller
    {
        // GET: Lesson2Form
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Index10()
        {   // 檢視畫面上的第一列（model）與 Model，有何不同？？

            Models.Class1 myClass = new Models.Class1();
            // 加入一筆記錄
            myClass.UserName = "Hello! The World.";

            return View(myClass);
            // 產生 View（檢視畫面）時，請選 Details範本
        }



        public ActionResult Index10BeginForm()
        {   // 搭配檢視畫面中的HTML表單，透過 HTML Helper來寫表單
            return View();
            // 產生一個「空白的」檢視畫面，自動動手寫表單

            // 若要搭配 「傳統HTML表單」來寫。
            // 請參閱上一個範例 Lesson1控制器底下的 Index91Add動作。
        }


        //--------------------------------------------------------------------------------------------------------
        public ActionResult Index11Create()
        {   // 搭配 Class1，做出新增（Create）的檢視畫面
            return View();
        }

        [ValidateAntiForgeryToken]   // 搭配檢視畫面（View）的Razor表單裡面，「@Html.AntiForgeryToken()」這句話以避免CSRF攻擊！！
        [HttpPost, ActionName("Index11Create")]    // ***重點！！刪除這一句話，會出錯！！***  完整寫法 [AcceptVerbs(HttpVerbs.Post)]
        public string Index11Create(Class1 class1)
        {   // 搭配 Class1，做出新增（Create）功能
            try
            {
                if (!ModelState.IsValid)   // 驗證（Validation）。預計到「第五節課」會講到
                {
                    return "NOT ModelState.IsValid";
                }
                return "成功！！......您剛剛輸入的 UserName是：<font color=blue>" + class1.UserName + "</font>";
            }
            catch
            {
                return "Exception：失敗";
            }
        }



        //--------------------------------------------------------------------------------------------------------
        //---- 目前還不連結資料庫。僅使用一個小類別檔（Class1.cs）來做 Model。
        //--------------------------------------------------------------------------------------------------------

        //******************************************************************
        public List<Models.Class1> myList = new List<Models.Class1>();
        //******************************************************************

        public ActionResult Index12Create()
        {   // 搭配 Class1，做出新增（Create）的檢視畫面
            return View();
        }

        [ValidateAntiForgeryToken]   // 搭配檢視畫面（View）的Razor表單裡面，「@Html.AntiForgeryToken()」這句話以避免CSRF攻擊！！
        [HttpPost]    // ***重點！！刪除這一句話，會出現錯誤！！***  完整寫法 [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index12Create(Class1 class1)
        {   // 搭配 Class1，做出新增（Create）功能
            try
            {
                if (!ModelState.IsValid)   // 驗證（Validation）。預計到「第五節課」會講到
                {
                    return View();
                }
                //畫面上，新增的字串
                myList.Add(new Class1 { UserName = "您剛剛輸入 - <font color=redn>" + class1.UserName + "</font>" });

                // 現在才加入的幾筆記錄
                myList.Add(new Class1 { UserName = "(5). 字串 ASP.NET" });
                myList.Add(new Class1 { UserName = "(6). 字串 Web Form" });
                myList.Add(new Class1 { UserName = "(7). 字串 MVC" });
                myList.Add(new Class1 { UserName = "(8). 字串 ASP.NET專題實務" });

                //*****************************************************
                return View("Index12List", myList);
                //--新增資料以後，要將 List<Models.Class1> 重新導向到 Index12List
                //*****************************************************
            }
            catch
            {
                return View();
            }
        }

        // 為什麼這個動作可以註解（不執行）呢？
        // 請對應 Lesson1底下的  Index1A與 Index1B 這兩個動作

        //public ActionResult Index12List()
        //{   // 搭配 Class1 列出、呈現（List）的檢視畫面
        //    return View(myList);
        //}


        //--------------------------------------------------------------------------------------------------------
        //---- 延續上面的範例，只修改第二個動作的「輸入值」，改為 FormCollection
        //--------------------------------------------------------------------------------------------------------
        public ActionResult Index13Create()
        {   // 搭配 Class1，做出新增（Create）的檢視畫面
            return View();
        }

        [ValidateAntiForgeryToken]   // 搭配檢視畫面（View）的Razor表單裡面，「@Html.AntiForgeryToken()」這句話以避免CSRF攻擊！！
        [HttpPost]    // ***重點！！刪除這一句話，會出現錯誤！！***
        public ActionResult Index13Create(Class1 class2)
        {   // 搭配 Class1，做出新增（Create）功能
            try
            {
                if (!ModelState.IsValid)   // 驗證（Validation）。預計到「第五節課」會講到
                {
                    return View();
                }

                myList.Add(new Class1 { UserName = "您剛剛輸入"+class2.UserName +"done" } );                
                return View("Index12List", myList);
                //--新增一筆資料以後，要重新導向到 Index12List
            }
            catch
            {
                return View();
            }
        }


        //--------------------------------------------------------------------------------------------------------
        //---- 延續上面的範例，只修改第二個動作的「輸入值」，改為 string
        //--------------------------------------------------------------------------------------------------------
        public ActionResult Index14Create()
        {   // 搭配 Class1，做出新增（Create）的檢視畫面
            return View();
        }

        [ValidateAntiForgeryToken]   // 搭配檢視畫面（View）的Razor表單裡面，「@Html.AntiForgeryToken()」這句話以避免CSRF攻擊！！
        [HttpPost]    // ***重點！！刪除這一句話，會出現錯誤！！***
        public ActionResult Index14Create(string UserName)
        {   // 搭配 Class1，做出新增（Create）功能
            try
            {
                if (!ModelState.IsValid)   // 驗證（Validation）。預計到「第五節課」會講到
                {
                    return View();
                }

                myList.Add(new Models.Class1 { UserName = "您剛剛輸入 - <font color=green>" + UserName + "</font>" });

                return View("Index12List", myList);
                //--新增一筆資料以後，要重新導向到 Index12List
            }
            catch
            {
                return View();
            }
        }


        //--------------------------------------------------------------------------------------------------------
        //--- C# Lambda 用法
        delegate int del(int i);

        public int Index15Lambda()
        {   // C# Lambda 用法       

            del myDelegate = (x => x * x);   // 計算 平方值
            int j = myDelegate(5);

            return j;
        }


        // 陣列的 .Any()函數。  找出（字串）陣列裡面，是否有「3」？
        // LINQ寫法：  字串陣列.Any(x => "關鍵字".Contains(x))
        public string Index16Lambda()
        {   // C# Lambda 用法       
            string str = "1,11,13,15,33,51";
            string[] strArray = str.Split(',');

            // 搜尋「3」 結果為 False
            //return strArray.Any(s => "3".Contains(s)).ToString();
            return strArray.Any("3".Contains).ToString();  // 這樣的寫法跟上一列程式相同。 // https://stackoverflow.com/questions/4191108/c-sharp-check-if-string-contains-any-matches-in-a-string-array

            // ***重點！！ ***字串陣列裡面的「字」，是否符合某一個字？ true or false
            // LINQ寫法：  字串陣列.Any(x => "關鍵字".Contains(x))  或是  字串陣列.Any("關鍵字".Contains)
        }


        public int Index17Lambda()
        {   // C# Lambda 用法       
            string[] words = { "9-blueberry", "3-ABC", "6-cherry", "5-apple", "6-banana", "10-WaterMelon" };

            // 計算陣列裡面，每個單字的 "字數"。選出長度最小的。
            int shortestWordLength = words.Min(w => w.Length);
            //**********************************************************

            return shortestWordLength;   // 答案是 5。也就是 "3-ABC"，陣列的第二個元素。
        }


        public int Index18Lambda()
        {   // C# Lambda 用法       
            string[] words = { "9-blueberry", "3-ABC", "6-cherry", "5-apple", "6-banana", "10-WaterMelon" };

            var query = from w in words
                        select w.Length;
            // query裡面的結果是 11, 5, 8, 7, 8, 12（陣列裡面 每一個元素 的 文字長度）

            // 用 .Min()方法找出 query裡面的最小值，答案是5。
            int shortestWordLength2 = query.Min();

            return shortestWordLength2;   // 答案是 5。也就是 "3-ABC"，陣列的第二個元素。
        }
    }
}