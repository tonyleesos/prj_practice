﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj_Practice_User.Models;
using prj_Practice_User.Models.Repo;

namespace prj_Practice_User.Controllers
{
    public class RepoController : Controller
    {
        IUserTableRepository MyDB = new UserTableRepository();   // 後面寫的是「類別名稱」
        // GET: Repo
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {
            IQueryable<UserTable> Listall = MyDB.ListAllUsers();
            if (Listall == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(Listall.ToList());
            }
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost, ActionName("Create")]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserTable _userTable)
        {
            if (_userTable != null && ModelState.IsValid)
            {
                if (MyDB.AddUser(_userTable))
                {
                    return RedirectToAction("List");
                }
            }

            return Content("新增失敗!!");
        }

        public ActionResult Delete(int ID)
        {
            UserTable DelItem = MyDB.GetUserByID(ID);
            if (DelItem == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(DelItem);
            }

        }
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComfirm(int ID)
        {
            if (ModelState.IsValid)
            {
                if (MyDB.DeleteUser(ID))
                {
                    return RedirectToAction("List");
                }
            }

            return Content("刪除失敗!!");
        }
    }
}