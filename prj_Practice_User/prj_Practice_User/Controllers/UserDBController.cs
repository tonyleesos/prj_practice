﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using prj_Practice_User.Models;

namespace prj_Practice_User.Controllers
{
    public class UserDBController : Controller
    {
        // GET: UserDB
        private MVC_UserDBContext _db = new MVC_UserDBContext();
        public ActionResult Index()
        {
            using (MVC_UserDBContext DB = new MVC_UserDBContext())
            {
                DB.UserTables.Select(x => x.UserName);
            }

            return View();
        }
        public ActionResult Create()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(UserTable user)
        {
            _db.UserTables.Add(user);
            _db.SaveChanges();
            return RedirectToAction("List");
        }
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            UserTable et = _db.UserTables.Find(id);

            if (et == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(et);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UserId, UserName, UserSex, UserBirthDay, UserMobilePhone")]UserTable user)
        {
            if (user == null)
            {   // 沒有輸入內容，就會報錯 - Bad Request
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }

            if (ModelState.IsValid)
            {
                UserTable Edittem = _db.UserTables.Find(user.UserId);

                Edittem.UserName = user.UserName;
                Edittem.UserSex = user.UserSex;
                Edittem.UserBirthDay = user.UserBirthDay;
                Edittem.UserMobilePhone = user.UserMobilePhone;

                _db.SaveChanges();
                return RedirectToAction("List");
            }
            else
            {
                ModelState.AddModelError("Value1", " 自訂錯誤訊息(1) ");
                ModelState.AddModelError("Value2", " 自訂錯誤訊息(2) ");
                return View();
            }
        }
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
            }
            UserTable dt = _db.UserTables.Find(id);

            if (dt == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(dt);
            }
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteComfirm(int id)
        {
            if (ModelState.IsValid)
            {
                UserTable DeleteItem = _db.UserTables.Find(id);
                _db.UserTables.Remove(DeleteItem);
                _db.SaveChanges();
                return RedirectToAction("List");
            }
            else
            {
                ModelState.AddModelError("Value1", " 自訂錯誤訊息(1) ");
                ModelState.AddModelError("Value2", " 自訂錯誤訊息(2) ");
                return View();
            }

        }
        public ActionResult List()
        {
            IQueryable<UserTable> ListAll = _db.UserTables;

            if (ListAll == null)
            {   // 找不到這一筆記錄
                return HttpNotFound();
            }
            else
            {
                return View(ListAll.ToList());
                // 直到程式的最後，把查詢結果 IQueryable呼叫.ToList()時，上面那一段LINQ才會真正被執行！
            }
        }
        [HttpGet]
        public ActionResult Details(int? id)
        {
            UserTable ListOne = _db.UserTables.Find(id);
            //ListOne.Where(m => m.UserId == _ID).FirstOrDefault();

            if (ListOne == null)
            {
                return HttpNotFound();
            }
            else
            {
                return View(ListOne);
            }

        }
        [HttpGet]
        public ActionResult Search(string SearchWord = "andy")
        {
            ViewData["SW"] = SearchWord;
            var ListAll = _db.UserTables.Where(x => x.UserName.Contains(SearchWord));
            return View(ListAll);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}