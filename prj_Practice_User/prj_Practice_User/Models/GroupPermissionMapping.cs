namespace prj_Practice_User.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("GroupPermissionMapping")]
    public partial class GroupPermissionMapping
    {
        public int Id { get; set; }

        public int? GroupId { get; set; }

        public int? PermissionId { get; set; }

        public virtual Group Group { get; set; }

        public virtual Permission Permission { get; set; }
    }
}
