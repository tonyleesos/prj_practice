namespace prj_Practice_User.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class LG : DbContext
    {
        public LG()
            : base("name=LG")
        {
        }

        public virtual DbSet<Group> Group { get; set; }
        public virtual DbSet<GroupPermissionMapping> GroupPermissionMapping { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserGroupMapping> UserGroupMapping { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Group>()
                .Property(e => e.GroupName)
                .IsFixedLength();

            modelBuilder.Entity<Permission>()
                .Property(e => e.Fun)
                .IsFixedLength();

            modelBuilder.Entity<User>()
                .Property(e => e.Name)
                .IsFixedLength();
        }
    }
}
