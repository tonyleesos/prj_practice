﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prj_Practice_User.Models.Repo
{
    interface IUserTableRepository
    {
        IQueryable<UserTable> ListAllUsers();
        UserTable GetUserByID(int id);
        IQueryable<UserTable> GetUserByName(string id);
        bool AddUser(UserTable _userTable);
        bool DeleteUser(int ID);

    }
}
