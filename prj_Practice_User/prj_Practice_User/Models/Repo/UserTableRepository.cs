﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using prj_Practice_User.Models;

namespace prj_Practice_User.Models.Repo
{
    public class UserTableRepository : IUserTableRepository, IDisposable
    {
        public MVC_UserDBContext _db = new MVC_UserDBContext();
        public bool AddUser(UserTable _userTable)
        {
            try
            {
                _db.UserTables.Add(_userTable);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteUser(int ID)
        {
            try
            {
                UserTable DelItem = _db.UserTables.Find(ID);
                _db.UserTables.Remove(DelItem);
                _db.SaveChanges();
                return true;
            }
            catch
            {
                return false;
            }
            throw new NotImplementedException();
        }

        UserTable IUserTableRepository.GetUserByID(int id)
        {
            throw new NotImplementedException();
        }

        IQueryable<UserTable> IUserTableRepository.GetUserByName(string id)
        {
            throw new NotImplementedException();
        }

        IQueryable<UserTable> IUserTableRepository.ListAllUsers()
        {
            IQueryable<UserTable> ListAll = _db.UserTables;
            return (ListAll);
            throw new NotImplementedException();
        }
        #region CloseDB
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {   // dispose managed resources
                _db.Dispose();
            }
            // free native resources
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}