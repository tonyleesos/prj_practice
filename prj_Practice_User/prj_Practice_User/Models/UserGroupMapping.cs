namespace prj_Practice_User.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("UserGroupMapping")]
    public partial class UserGroupMapping
    {
        public int Id { get; set; }

        public int? UserId { get; set; }

        public int? GroupId { get; set; }

        public virtual Group Group { get; set; }

        public virtual User User { get; set; }
    }
}
