﻿using prj_Practice_UserConnect.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace prj_Practice_UserConnect.Controllers
{
    public class UserDB2Controller : Controller
    {
        public MVC_UserDBContext _db = new MVC_UserDBContext();
        // GET: UserDB2
        public ActionResult Index()
        {
            return View(_db.UserTable2s.ToList());
        }
        public ActionResult IndexMasterDetials5()
        {
            var result = from d in _db.DepartmentTable2s
                         join u in _db.UserTable2s on d.DepartmentId equals u.DepartmentId
                         orderby d.DepartmentId
                         select new UserDepartmentViewModel { DepartmentVM = d, UserVM = u };
            return View(result);
        }
        public ActionResult IndexJOIN()
        {
            var resultViewModel = from u in _db.UserTable2s
                                  join d in _db.DepartmentTable2s on u.DepartmentId equals d.DepartmentId into dt2
                                  from d in dt2.DefaultIfEmpty()
                                  select new UserDepartmentViewModel { DepartmentVM = d, UserVM = u };
            return View(resultViewModel);
        }
        public ActionResult IndexJOINDetails(int ID = 4)
        {
            var resultViewModel = from u in _db.UserTable2s
                                  join d in _db.DepartmentTable2s on u.DepartmentId equals d.DepartmentId into dt2
                                  from d in dt2.DefaultIfEmpty()
                                  where d.DepartmentId == ID
                                  select new UserDepartmentViewModel { DepartmentVM = d, UserVM = u };
            return View(resultViewModel);
        }
        public ActionResult IndexVM1()
        {
            List<UDViewModel> resultVM = new List<UDViewModel>();
            foreach (var dt in _db.DepartmentTable2s)
            {
                List<UserTable2> ut = new List<UserTable2>();
                foreach (var it in _db.UserTable2s.Where(x => x.DepartmentId == dt.DepartmentId))
                {
                    ut.Add(it);
                }
                resultVM.Add(new UDViewModel { DVM = dt, UVM = ut });
            }
            return View(resultVM.ToList());
        }
        public ActionResult IndexVM2()
        {
            UDmultiViewModel resultVM = new UDmultiViewModel
            {
                DVM = _db.DepartmentTable2s.ToList(),
                UVM = _db.UserTable2s.ToList()
            };

            return View(resultVM);
        }
    }
}