namespace prj_Practice_UserConnect.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class MVC_UserDBContext : DbContext
    {
        public MVC_UserDBContext()
            : base("name=MVC_UserDBContext")
        {
        }

        public virtual DbSet<DepartmentTable2> DepartmentTable2s { get; set; }
        public virtual DbSet<UserTable2> UserTable2s { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserTable2>()
                .Property(e => e.UserSex)
                .IsFixedLength();
        }
    }
}
