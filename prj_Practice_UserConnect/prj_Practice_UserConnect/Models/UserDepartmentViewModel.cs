﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace prj_Practice_UserConnect.Models
{
    public class UserDepartmentViewModel
    {
        public UserTable2 UserVM { get; set; }
        public DepartmentTable2 DepartmentVM { get; set; }
    }
}